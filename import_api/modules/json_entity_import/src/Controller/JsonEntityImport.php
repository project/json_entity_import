<?php

namespace Drupal\json_entity_import\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class RoiFormListController.
 */
class JsonEntityImport extends ControllerBase {
  /**
   * Salesforce client.
   *
   * @var \Drupal\salesforce\Rest\RestClientInterface
   */
  protected $client;

  public function __construct(RestClientInterface $client) {
    $this->client = $client;
  }

  public static function create(ContainerInterface $container) {
    $client = $container->get('salesforce.client');
    return new static($client);
  }

  /**
   *
   * @return array
   *   An Array containing the table.
   */
  public function dashboard() {
    $build = [
      '#markup' => '<link href="https://www.w3schools.com/w3css/4/w3.css" rel="stylesheet" /><p>
      <a class="w3-button w3-black" href="https://www.w3schools.com">Check token</a> 
      <a class="w3-button w3-black" href="/admin/config/salesforce/import_data">Import webform and salesforce Mapping</a> 
      </p>',
    ];
    return $build;
  }

  /**
   *
   * @return array
   *   An Array containing the table.
   */
  public function importConfiguration() {
    $method = 'objects';
    $arg = [[], TRUE];
    if ($this->client->isInit()) {
      try {
        call_user_func_array([$this->client, $method], $arg);
        return TRUE;
      }
      catch (\Exception $e) {
        // Fall through.
        $message = $e->getMessage() ?: get_class($e);
      }
    }

    $href = new Url('salesforce.auth_config');

    $build = [
      '#markup' => 'webform import successfully',
    ];
    return $build;
  }

}
