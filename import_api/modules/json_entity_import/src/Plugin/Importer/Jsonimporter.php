<?php

namespace Drupal\json_entity_import\Plugin\Importer;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\import_api\BatchStatus;
use Drupal\import_api\Plugin\ImporterPluginBase;
use Drupal\node\Entity\Node;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Importer(
 *   id = "json_import_entity_importer",
 *   label = @Translation("entity Importer"),
 *   category = @Translation("Import API examples"),
 *   format = "json",
 * )
 */
class Jsonimporter extends ImporterPluginBase implements ContainerFactoryPluginInterface {

  /**
   * @var EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Jsonimporter constructor.
   *
   * @param EntityTypeManagerInterface $entityTypeManager
   * @param string $configuration
   * @param mixed $plugin_id
   * @param $plugin_definition
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $container->get('entity_type.manager'),
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function batch($data, BatchStatus $batch_status, &$context) {
    foreach ($data['data'] as $index => $paragraph) {
      $content_type = \Drupal::config('json_entity_import.settings')->get('content_type');
      $node = Node::create([
        'type' => $content_type,
        'title' => $paragraph['title'],
        'body' => $paragraph['body'],
        // 'body' => $paragraph['label'],
        // 'field_risd_id' => $index,
      ]);

      $node->save();

      $batch_status
        ->setCurrent($index)
        ->addResult($index)
        ->setMessage(new TranslatableMarkup('Importing: @title', [
          '@title' => $title,
        ]))
        ->incrementProgress();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function fetch($context) {
    $client = new Client();
    $json_file = \Drupal::config('json_entity_import.settings')->get('json_file');
    $response = $client->request('GET', $json_file);

    return $response->getBody()->getContents();
  }

  /**
   * {@inheritdoc}
   */
  public function getTotal($data) {
    return count($data);
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoveBatchTotal() {
    $content_type = \Drupal::config('json_entity_import.settings')->get('content_type');
    $query = \Drupal::entityQuery('node')
      ->condition('type', $content_type);

    $query->count();

    return (int) $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function removeQuery(&$context) {
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'infographics')
      ->condition('nid', $context['sandbox']['current_id'], '>')
      ->sort('nid')
      ->range(0, 10);

    $node_ids = $query->execute();

    $nodes = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->loadMultiple($node_ids);

    return $nodes;
  }

  /**
   * {@inheritdoc}
   */
  public function removeBatch($data, $context) {
    /** @var NodeInterface $node */
    foreach ($data as $node) {
      $context['results']['remove'][] = $node->id() . ':' . $node->label();

      $context['sandbox']['progress']++;
      $context['sandbox']['current_id'] = $node->id();

      $context['message'] = new TranslatableMarkup('Removing @label', [
        '@label' => $node->label(),
      ]);

      $node->delete();
    }
  }

}
