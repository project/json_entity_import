<?php

namespace Drupal\json_entity_import\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures forms module settings.
 */
class ModuleConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'json_entity_import_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'json_entity_import.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('json_entity_import.settings');
    $form['json_file'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL of json file'),
      '#default_value' => $config->get('json_file'),
    ];
    $form['content_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Content Type name'),
      '#default_value' => $config->get('content_type'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('json_entity_import.settings')
      ->set('json_file', $form_state->getValue('json_file'))
      ->set('content_type', $form_state->getValue('content_type'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}

